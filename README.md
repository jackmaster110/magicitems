
# MagicItems
MagicItems is a Spigot plugin that adds some cool magical items into your game. You can download the newest compiled .jar from [Spigot](https://www.spigotmc.org/resources/magic-items.56730/).

The default `config.yml` is:
```YAML
Gillyweed_Effect_Duration: 15  
InvisibilityDust_Effect_Duration: 15  
Flight_Duration: 5
```

### Command Help:
**/magicitems** - The default command for the plugin (/mi, /mitems)
**/magicitems give [item-name] [player] [amount]** - Give a player an item
**/magicitems open** - Open the GUI to get items
**/magicitems recipe [item-name]** - A command that will allow you to view recipes once they are added.

**[item-name] List:**

 - gillyweed
 - invisibility
 - flight

If you have any questions or bug reports, just let me know.

**Note: Maintaining this plugin is not my top priority by any means, and there may be a point where I stop maintaining it entirely.**