package tk.jackmaster110.MagicItems.items.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import tk.jackmaster110.MagicItems.items.GUIs;

import java.util.UUID;

public class GUIListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (!(e.getWhoClicked() instanceof Player)) {
			return;
		}
		Player p = (Player) e.getWhoClicked();
		UUID pUUID = (UUID) p.getUniqueId();

		UUID inventoryUUID = GUIs.openInventories.get(pUUID);
		if (inventoryUUID != null) {
			e.setCancelled(true);

			GUIs gui = GUIs.getInventoriesByUUID().get(inventoryUUID);
			GUIs.inventoryAction action = gui.getActions().get(e.getSlot());

			if (action != null) {
				action.click(p);
			}
		}
	}


	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		UUID pUUID = p.getUniqueId();

		GUIs.openInventories.remove(pUUID);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = (Player) e.getPlayer();
		UUID pUUID = p.getUniqueId();

		GUIs.openInventories.remove(pUUID);
	}

}
