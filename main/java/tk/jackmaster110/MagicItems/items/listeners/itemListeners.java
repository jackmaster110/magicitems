package tk.jackmaster110.MagicItems.items.listeners;

import com.codingforcookies.armorequip.ArmorEquipEvent;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.jackmaster110.MagicItems.FileManager;
import tk.jackmaster110.MagicItems.Main;
import tk.jackmaster110.MagicItems.items.Items;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class itemListeners implements Listener {

	private Main plugin;

	public itemListeners(Main mainInstance) {
		this.plugin = mainInstance;
	}

	@EventHandler
	public void onRightClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		ItemStack item = e.getItem();
		ItemMeta iMeta = item.getItemMeta();

		if (a.equals(Action.RIGHT_CLICK_AIR) || a.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType().equals(Material.SLIME_BALL) && iMeta.equals(Items.gillyweed.getItemMeta())) {
				if (!p.hasPermission("mi.gillyweed")) {
					p.sendMessage(ChatColor.RED + "You don't have permission to use gillyweed!");
					return;
				} else {
					int duration = FileManager.config.getInt("Gillyweed_Effect_Duration") * 20;
					if (p.hasPotionEffect(PotionEffectType.WATER_BREATHING)) {
						duration = duration + p.getPotionEffect(PotionEffectType.WATER_BREATHING).getDuration();
						p.removePotionEffect(PotionEffectType.WATER_BREATHING);
						p.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, duration, 255));
						p.getInventory().removeItem(Items.gillyweed);
						p.sendMessage(ChatColor.GREEN + "You've used your gillyweed.");
						Bukkit.getLogger().info(p.getName() + " has used gillyweed.");
					}
					p.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, duration, 255));
					p.getInventory().removeItem(Items.gillyweed);
					p.sendMessage(ChatColor.GREEN + "You've used your gillyweed.");
					Bukkit.getLogger().info(p.getName() + " has used gillyweed.");
				}
			} else if (item != null && item.getType().equals(Material.SULPHUR) && item.getItemMeta().equals(Items.invisibility.getItemMeta())) {
				if (!p.hasPermission("mi.invisibility")) {
					p.sendMessage(ChatColor.RED + "You don't have permission to use invisibiility dust!");
					return;
				} else {
					int duration = FileManager.config.getInt("InvisibilityDust_Effect_Duration") * 20;
					if (p.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
						duration = duration + p.getPotionEffect(PotionEffectType.INVISIBILITY).getDuration();
						p.removePotionEffect(PotionEffectType.INVISIBILITY);
						p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, duration, 255));
						p.getInventory().removeItem(Items.invisibility);
						p.sendMessage(ChatColor.GREEN + "You've used your invisibility dust.");
						Bukkit.getLogger().info(p.getName() + " has used invisibility dust.");
					}
					p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, duration, 255));
					p.getInventory().removeItem(Items.invisibility);
					p.sendMessage(ChatColor.GREEN + "You've used your invisibility dust.");
					Bukkit.getLogger().info(p.getName() + " has used invisibility dust.");
				}
			} else if (item != null && item.getType().equals(Material.SUGAR) && item.getItemMeta().equals(Items.flight.getItemMeta())) {
				if (!p.hasPermission("mi.flight")) {
					p.sendMessage(ChatColor.RED + "You don't have permission to use flight powder!");
				} else {
					int duration = FileManager.config.getInt("Flight_Duration");
					if (p.getAllowFlight()) {
						p.sendMessage(ChatColor.RED + "You already have this ability active!");
					} else {
						p.setAllowFlight(true);
						p.getInventory().removeItem(Items.flight);
						p.sendMessage(ChatColor.GREEN + "Activated flight for " + duration + " seconds.");

						Timer t = new Timer();
						TimerTask timerTask = new TimerTask() {
							@Override
							public void run() {
								p.setAllowFlight(false);
								p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 40, 255));
								p.sendMessage(ChatColor.RED + "Deactivating flight...");
								Bukkit.getLogger().info(p +"'s flight just ran out.");
							}
						};
						t.schedule(timerTask, duration*1000);
					}
				}
			}
		}
	}
}