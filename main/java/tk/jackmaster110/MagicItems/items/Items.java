package tk.jackmaster110.MagicItems.items;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Items {
	public static void giveItem(ItemStack item, Player p, int amount) {
		ItemStack is = new ItemStack(item.getType(), amount);
		ItemMeta isMeta = item.getItemMeta();
		is.setItemMeta(isMeta);
		p.getInventory().addItem(is);
	}

	public static ItemStack newItem(Material material, String name, String lore1, String lore2, ChatColor namecolor) {
		ItemStack is = new ItemStack(material, 1);
		ItemMeta meta = is.getItemMeta();
		List<String> lore = new ArrayList<String>();
		if (lore1 != null) {
			lore.add(lore1);
		}
		if (lore2 != null) {
			lore.add(lore2);
		}
		meta.setLocalizedName(namecolor + name);
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}
	public static ItemStack enchantItem(ItemStack is, Enchantment e, int power) {
		is.addEnchantment(e, power);
		return is;
	}

	public static Map<String, ItemStack> itemList = new HashMap<String, ItemStack>();

	public static ItemStack gillyweed = newItem(Material.SLIME_BALL, "Gillyweed", "Use this to breathe underwater!", null, ChatColor.GREEN);
	public static ItemStack invisibility = newItem(Material.SULPHUR, "Invisibility Dust", "Use this to turn invisible temporarily!", ChatColor.GOLD + "" + ChatColor.BOLD + "Under a fancy enchantment", ChatColor.AQUA);
	public static ItemStack flight = newItem(Material.SUGAR, "Flight Power", "Use this for temporary flight!", null, ChatColor.RED);

	public static void addList() {
		itemList.put("gillyweed", gillyweed);
		itemList.put("invisibility", invisibility);
		itemList.put("flight", flight);
	}


}
