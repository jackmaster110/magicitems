package tk.jackmaster110.MagicItems.items;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class GUIs {

	public static Map<UUID, GUIs> inventoriesByUUID = new HashMap<UUID, GUIs>();
	public static Map<UUID, UUID> openInventories = new HashMap<UUID, UUID>();

	private UUID uuid;
	private Inventory inventory;
	private Map<Integer, inventoryAction> actions;

	public GUIs(int invSize, String invName) {
		uuid = UUID.randomUUID();
		inventory = Bukkit.createInventory(null, invSize, invName);
		actions = new HashMap<Integer, inventoryAction>();
		inventoriesByUUID.put(getUuid(), this);
	}

	public UUID getUuid() {
		return uuid;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public interface inventoryAction {
		void click(Player p);
	}

	public void setItem(int slot, ItemStack is, inventoryAction action) {

		inventory.setItem(slot, is);
		if (action != null) {
			actions.put(slot, action);
		}

	}

	public void setItem(int slot, ItemStack is) {
		setItem(slot, is, null);
	}

	public void openGUI(Player p) {
		p.openInventory(inventory);
		openInventories.put(p.getUniqueId(), getUuid());
	}

	public static Map<UUID, GUIs> getInventoriesByUUID() {
		return inventoriesByUUID;
	}

	public static Map<UUID, UUID> getOpenInventories() {
		return openInventories;
	}

	public Map<Integer, inventoryAction> getActions() {
		return actions;
	}

	public void delete() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			UUID u = openInventories.get(p.getUniqueId());
			if (u.equals((getUuid()))) {
				p.closeInventory();
			}
		}
		inventoriesByUUID.remove(getUuid());
	}

	public void closeGUI(Player p) {
		p.closeInventory();
		delete();
	}

}
