package tk.jackmaster110.MagicItems.items;

import org.bukkit.ChatColor;

import java.util.UUID;

public class mItemGUI extends GUIs {

	public mItemGUI() {
		super(9, "Magic Items");

		setItem(2, Items.gillyweed, p -> {
			p.getInventory().addItem(Items.gillyweed);
			p.sendMessage(ChatColor.GREEN + "Giving gillyweed");

			//Close the inventory
			UUID pUUID = p.getUniqueId();
			GUIs.openInventories.remove(pUUID);
			closeGUI(p);
		});
		setItem(4, Items.invisibility, p -> {
			p.getInventory().addItem(Items.invisibility);
			p.sendMessage(ChatColor.GREEN + "Giving invisibility dust.");

			//Close the inventory
			UUID pUUID = p.getUniqueId();
			GUIs.openInventories.remove(pUUID);
			closeGUI(p);
		});
		setItem(6, Items.flight, p -> {
			p.getInventory().addItem(Items.flight);
			p.sendMessage(ChatColor.GREEN + "Giving flight powder.");

			//Close the inventory
			UUID pUUID = p.getUniqueId();
			GUIs.openInventories.remove(pUUID);
			closeGUI(p);
		});
	}
}
