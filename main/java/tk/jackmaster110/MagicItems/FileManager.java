package tk.jackmaster110.MagicItems;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.YamlConfigurationOptions;

import java.io.File;
import java.io.IOException;

public class FileManager {

	public static FileConfiguration config;
	private static File configFile;

	static Main plugin;
	public FileManager(Main pluginX) {
		plugin = pluginX;

		configFile = new File(plugin.getDataFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(configFile);
		try {
			config.options().copyDefaults(true);
			config.addDefault("Gillyweed_Effect_Duration", 15);
			config.addDefault("InvisibilityDust_Effect_Duration", 15);
			config.addDefault("Flight_Duration", 5);
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
