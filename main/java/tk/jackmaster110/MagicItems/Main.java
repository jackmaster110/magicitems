package tk.jackmaster110.MagicItems;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import tk.jackmaster110.MagicItems.items.Items;
import tk.jackmaster110.MagicItems.items.listeners.GUIListener;
import tk.jackmaster110.MagicItems.items.listeners.armorListeners;
import tk.jackmaster110.MagicItems.items.listeners.itemListeners;
import tk.jackmaster110.MagicItems.items.mItemGUI;

import java.util.Map;

public class Main extends JavaPlugin implements Listener {

	private mItemGUI mItemsGUI;
	public static boolean pluginActive;
	public Plugin magicItems;
	@Override
	public void onEnable() {
		mItemsGUI = new mItemGUI();
		final PluginManager pm = getServer().getPluginManager();
		this.magicItems = pm.getPlugin("MagicItems");
		getLogger().info("MagicItems by jackmaster110 has been enabled");
		Bukkit.getServer().getPluginManager().registerEvents(new itemListeners(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new GUIListener(), this);
		Items.addList();
		pluginActive = true;

		//config.yml setup
		new FileManager(this);
		FileConfiguration config = FileManager.config;
		config.addDefault("Gillyweed_Effect_Duration", 15);
		config.addDefault("InvisibilityDust_Effect_Duration", 15);
		config.addDefault("Flight_Duration", 5);
		saveDefaultConfig();
	}

	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		saveConfig();
	}

	@Override
	public void onDisable() {
		getLogger().info("MagicItems by jackmaster110 has been disabled");
		pluginActive = false;
	}

	public PluginDescriptionFile pdf = this.getDescription();

	@Override
	public boolean onCommand(CommandSender sender,
	                         Command cmd,
	                         String label,
	                         String[] args) {
		if (cmd.getName().equalsIgnoreCase("magicitems")) {
			if (args.length < 1) {
				sender.sendMessage(ChatColor.BLUE + "MagicItems version " + pdf.getVersion() + " by " + pdf.getAuthors() + ".");
				sender.sendMessage(ChatColor.BLUE + "Do /magicitems help for command help.");
			}
			if (args[0].equalsIgnoreCase("help")) {
				sender.sendMessage(ChatColor.BLUE + "MagicItems Help: \n " +
						"/magicitems = The base command for all of MagicItems (Alias: /mi) \n " +
						"/magicitems help = Display this message \n " +
						"/magicitems open = Open the GUI where you can get magic Magic Items \n " +
						"/magicitems give <item> [player] [amount] = Give a magic item to a player \n" +
						"/magicitems recipe <item> = Get the crafting recipe for a magic item.");
			}
			if (args[0].equalsIgnoreCase("open") && sender.hasPermission("mi.open")) {
				Player p = (Player) sender;
				mItemsGUI.openGUI(p);
			}
			if (args[0].equalsIgnoreCase("give") && sender.hasPermission("mi.give")) {
				for (Map.Entry<String, ItemStack> entry : Items.itemList.entrySet()) {
					String name = entry.getKey();
					ItemStack is = entry.getValue();

					if (args[1].equalsIgnoreCase(name) && args.length < 3 && sender instanceof Player) {
						Player p = (Player) sender;
						Items.giveItem(is, p, 1);
					} else if (args[1].equalsIgnoreCase(name) && args[2] != null && args.length < 4) {
						Player target = Bukkit.getServer().getPlayer(args[2]);
						sender.sendMessage(ChatColor.GREEN + "Giving " + name + " to " + target.getName());
						Items.giveItem(is, target, 1);
						target.sendMessage(ChatColor.GREEN + "You were given " + name);
					} else if (args[1].equalsIgnoreCase(name) && args[3] != null && args.length < 5) {
						if (!StringUtils.isNumeric(args[3])) {
							sender.sendMessage(ChatColor.RED + args[3] + " is not numeric!");
						}
						else {
							int amount = Integer.parseInt(args[3]);
							Player target = Bukkit.getServer().getPlayer(args[2]);
							sender.sendMessage(ChatColor.RED + "Giving " + amount + " of " + name + " to " + target.getName());
							target.sendMessage(ChatColor.GREEN + "You were given " + amount + " of " + name);
							Items.giveItem(is, target, amount);
						}
					} else if (args.length >= 6){
						sender.sendMessage(ChatColor.RED + "Invalid number of arguments!");
					}
				}
			}
			if (args[0].equalsIgnoreCase("recipe")) {
				sender.sendMessage(ChatColor.RED + "That command is unavailable at this time. Sorry for the inconvenience.");
			}
			return true;
		}
		return false;
	}

}
